package fr.imt_atlantique.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class UserInformationActivity extends AppCompatActivity {

    private TextView userInformationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_information);

        userInformationText = findViewById(R.id.userInformation);
        displayUserInformation();
    }

    public void displayUserInformation() {
        Intent intent = getIntent();
        if (intent != null){
            User user = intent.getParcelableExtra("user");
            if (user != null){
                String userInformation = user.getFirstname() + " " + user.getLastname() + " is born in " + user.getBirthCity();
                if(user.isBirthDepartmentProvided()) {
                    userInformation += " located in " + user.getBirthDepartment();
                }
                if(user.isBirthDateProvided()) {
                    userInformation += " in " + user.getBirthDate();
                }
                if(!user.isPhoneNumberEmpty()) {
                    ArrayList<String> phoneNumbers = user.getPhoneNumbers();
                    userInformation += "\nFind below phone number(s):";
                    for(int i=0;i<phoneNumbers.size();i++) {
                        userInformation += "\n" + phoneNumbers.get(i);
                    }
                }
                userInformationText.setText(userInformation);
            }
        }
    }
}
