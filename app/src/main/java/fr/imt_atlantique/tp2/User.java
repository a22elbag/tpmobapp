package fr.imt_atlantique.tp2;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class User implements Parcelable {
    private String lastname;
    private String firstname;
    private String birthCity;
    private String birthDate;
    private String birthDepartment;
    private ArrayList<String> phoneNumbers;

public  User(String lastname, String firstname, String birthCity, String birthDate, String birthDepartment, ArrayList<String> phoneNumbers) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthCity = birthCity;
        this.birthDate = birthDate;
        this.birthDepartment = birthDepartment;
        this.phoneNumbers = phoneNumbers;
    }

    protected User(Parcel in) {
        this.lastname = in.readString();
        this.firstname = in.readString();
        this.birthCity = in.readString();
        this.birthDate = in.readString();
        this.birthDepartment = in.readString();
        this.phoneNumbers = in.readArrayList(null);
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getBirthCity() {
        return this.birthCity;
    }

    public String getBirthDate() { return this.birthDate;}

    public String getBirthDepartment() { return this.birthDepartment;}

    public ArrayList<String> getPhoneNumbers() { return this.phoneNumbers;}

    public boolean isBirthDateProvided() { return !(this.birthDate.replaceAll("\\s+","").equals(""));}

    public boolean isBirthDepartmentProvided() { return !(this.birthDepartment.equals("Choose department"));}

    public boolean isPhoneNumberEmpty() { return this.phoneNumbers.size() == 0;}


    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.lastname);
        out.writeString(this.firstname);
        out.writeString(this.birthCity);
        out.writeString(this.birthDate);
        out.writeString(this.birthDepartment);
        out.writeList(this.phoneNumbers);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // After implementing the `Parcelable` interface, we need to create the
    // `Parcelable.Creator<MyParcelable> CREATOR` constant for our class;
    // Notice how it has our class specified as its type.
    public static final Parcelable.Creator<User> CREATOR
            = new Parcelable.Creator<User>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
