package fr.imt_atlantique.tp2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TwoLineListItem;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button validateButton;
    private EditText lastnameEditText;
    private EditText firstnameEditText;
    private static EditText birthdayEditText;
    private EditText cityEditText;
    private Button addPhoneNumberButton;
    private EditText phoneNumberEditText;
    private ListView numbersListView;
    private Button pickDateButton;

    private Spinner departmentSpinner;

    private ArrayList<String> numbers;
    private ArrayAdapter<String> numberAdapter;
    private String firstnameValue;
    private String lastnameValue;
    private String phoneNumberValue;
    private String cityValue;
    private String departmentValue;
    private String birthdayValue;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        validateButton = (Button) findViewById(R.id.buttonValidate);
        lastnameEditText = findViewById(R.id.editTextNom);
        firstnameEditText = findViewById(R.id.editTextPrenom);
        birthdayEditText = findViewById(R.id.editTextDate);
        cityEditText = findViewById(R.id.editTextVille);
        addPhoneNumberButton = (Button) findViewById(R.id.buttonAdd);
        phoneNumberEditText = findViewById(R.id.editTextPhone);
        departmentSpinner = findViewById(R.id.department_spinner);
        numbersListView = findViewById(R.id.list_view);
        pickDateButton = findViewById(R.id.pickDateButton);

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        firstnameValue = sharedPreferences.getString("firstname", " ");
        firstnameValue = sharedPreferences.getString("lastname", " ");
        cityValue = sharedPreferences.getString("city", " ");
        departmentValue = sharedPreferences.getString("department", " ");

        birthdayValue = sharedPreferences.getString("birthday", " ");
        phoneNumberValue = sharedPreferences.getString("phone", " ");

        numbers = new ArrayList<>();
        numberAdapter = new ArrayAdapter<String>(this, R.layout.phone_number_layout, android.R.id.text1, numbers);
        numbersListView.setAdapter(numberAdapter);

        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveUserInformation(editor);
                shareUserToUserInformationActivity();
            }
        });

        addPhoneNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = phoneNumberEditText.getText().toString();
                if(!(phoneNumber.replaceAll("\\s+","").equals(""))) {
                    numbers.add(phoneNumber);
                    numberAdapter.notifyDataSetChanged();
                    phoneNumberEditText.setText("");
                } else {
                    displaySnackbar("No number given");
                }

            }
        });

        pickDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dateIntent = new Intent();
                dateIntent.setAction(Intent.ACTION_PICK);
                dateIntent.putExtra("currentDate", birthdayEditText.getText().toString()); // la clé, la valeur
                startActivityForResult(dateIntent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1)
        {
            String message=data.getStringExtra("date");
            birthdayEditText.setText(message);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void shareUserToUserInformationActivity() {
        if(isUserInformationNotGiven()) {
            displaySnackbar("Not all needed user information are given");
        } else {
            Intent userInformationIntent = new Intent(MainActivity.this, UserInformationActivity.class);
            User user = new User(lastnameValue, firstnameValue, cityValue, birthdayValue, departmentValue, numbers);
            userInformationIntent.putExtra("user", user); // la clé, la valeur
            startActivity(userInformationIntent);
        }
    }

    public void saveUserInformation(SharedPreferences.Editor editor) {
        firstnameValue = firstnameEditText.getText().toString();
        lastnameValue = lastnameEditText.getText().toString();
        phoneNumberValue = phoneNumberEditText.getText().toString();
        cityValue = cityEditText.getText().toString();
        departmentValue = departmentSpinner.getSelectedItem().toString();
        birthdayValue = birthdayEditText.getText().toString();

        editor.putString("firstname", firstnameValue);
        editor.putString("lastname", lastnameValue);
        editor.putString("phone", phoneNumberValue);
        editor.putString("city", cityValue);
        editor.putString("department", departmentValue);
        editor.putString("birthday", birthdayValue);
        editor.commit();
        editor.apply();
    }

    public boolean isUserInformationNotGiven() {
        return lastnameValue.replaceAll("\\s+","").equals("") ||
                firstnameValue.replaceAll("\\s+","").equals("") ||
                cityValue.replaceAll("\\s+","").equals("");
    }

    //function used in main_menu xml
    public void researchCityOnWikipedia(MenuItem item) {
        if(isCityTextNotGiven()) {
            displaySnackbar("No city given");
        } else {
            String wikipediaUrl = "http://fr.wikipedia.org/?search=" + cityEditText.getText().toString();
            Intent wikipediaIntent = new Intent(Intent.ACTION_VIEW);
            wikipediaIntent.setData(Uri.parse(wikipediaUrl));
            startActivity(wikipediaIntent);
        }
    }

    //function used in main_menu xml
    public void shareCity(MenuItem item) {
        if(isCityTextNotGiven()) {
            displaySnackbar("No city given");
        } else {
            // Create the text message with a string.
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, cityEditText.getText().toString());
            sendIntent.setType("text/plain");

            // Try to invoke the intent.
            try {
                startActivity(sendIntent);
            } catch (ActivityNotFoundException e) {
                Log.i("Lifecycle", "No application available for such intent");
            }
        }
    }

    public boolean isCityTextNotGiven() {
        return cityEditText.getText().toString().replaceAll("\\s+","").equals("");
    }

    public void displaySnackbar(String Message) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.myConstraintLayout), Message, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    //function used in main_menu xml
    public void resetAction (MenuItem item){
        lastnameEditText.setText("");
        firstnameEditText.setText("");
        birthdayEditText.setText("");
        cityEditText.setText("");
        phoneNumberEditText.setText("");
        departmentSpinner.setSelection(0);
        numbers = new ArrayList<>();
        numberAdapter = new ArrayAdapter<String>(this, R.layout.phone_number_layout, android.R.id.text1, numbers);
        numbersListView.setAdapter(numberAdapter);
    }

    //function used in phone_number_layout.xml
    public void removeNumber(View v) {
        ListView lv = (ListView) v.getParent().getParent().getParent();
        int position = lv.getPositionForView((TwoLineListItem)v.getParent().getParent());
        numbers.remove(position);
        numberAdapter = new ArrayAdapter<String>(this, R.layout.phone_number_layout, android.R.id.text1, numbers);
        numbersListView.setAdapter(numberAdapter);
    }

    public void dialPhoneNumber(View v) {
        ListView lv = (ListView) v.getParent().getParent().getParent();
        int position = lv.getPositionForView((TwoLineListItem)v.getParent().getParent());
        String phoneNumber = numbers.get(position);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_DIAL);
        sendIntent.setData(Uri.parse("tel:" + phoneNumber));
      //  sendIntent.setType("text/plain");

        // Try to invoke the intent.
        try {
            startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {
            Log.i("Lifecycle", "No application available for such intent");
        }
    }
}

