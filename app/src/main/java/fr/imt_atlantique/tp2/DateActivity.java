package fr.imt_atlantique.tp2;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateActivity extends AppCompatActivity {

    private static TextView birthdayTextView;
    private Button validateButton;
    private Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        this.birthdayTextView = findViewById(R.id.dateTextView);
        this.validateButton = findViewById(R.id.validateDateButton);
        this.cancelButton = findViewById(R.id.cancelDateSelectionButton);

        Intent intent = getIntent();
        if (intent != null) {
            String givenDate = intent.getStringExtra("currentDate");
            if(givenDate != null) {
                Log.i("dateIntent", givenDate);
                if(!(givenDate.replaceAll("\\s+","").equals(""))) {
                    birthdayTextView.setText(givenDate);
                }
            }
        }

        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stringToReturn ="";
                if(!(birthdayTextView.getText().toString().equals("Choose a date"))) {
                    stringToReturn = birthdayTextView.getText().toString();
                }
                Intent dateIntent = new Intent(DateActivity.this, MainActivity.class);
                dateIntent.putExtra("date", stringToReturn); // la clé, la valeur
                setResult(1,dateIntent);
                finish();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stringToReturn ="";
                if(!(birthdayTextView.getText().toString().equals("Choose a date"))) {
                    stringToReturn = birthdayTextView.getText().toString();
                }
                Intent dateIntent = new Intent(DateActivity.this, MainActivity.class);
                dateIntent.putExtra("date", stringToReturn); // la clé, la valeur
                setResult(1,dateIntent);
                finish();
            }
        });
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new DatePickerDialog(requireContext(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String dateString = sdf.format(c.getTime());
            birthdayTextView.setText(dateString);
        }

    }
    public void showDatePickerDialog(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
}
